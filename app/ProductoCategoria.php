<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductoCategoria extends Model
{
    protected $fillable = [
        "id_producto",
        "id_categoria"
    ];

    public function producto()
    {
        return $this->belongsTo('App\Producto');
    }

    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }
}
