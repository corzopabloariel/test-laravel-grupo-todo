<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoria extends Model
{
    protected $fillable = [
        "nombre",
        "descripcion",
        "categoria_id"
    ];

    public function categorias()
    {
        return $this->hasMany('App\Categoria', 'categoria_id');
    }
    public function categoria()
    {
        return $this->belongsTo('App\Categoria');
    }
    /**
     * Función recursiva que busca los padres de una categoría
     *@return Array
     */
    public function padresRecursivo( $data , &$padres , $only_id ) {
        //dd($data->padre);
        if( !empty( $data->categoria_id ) ) {
            if( !empty( $only_id ) )
                $padres[] = $data->categoria;
            else
                $padres[] = $data->categoria_id;
            self::padresRecursivo( $data->categoria , $padres , $only_id );
        }
    }
    /**
     * Función recursiva que busca los hijos de una categoría
     *@return Array
     */
    public function hijosRecursivo( $data , &$hijos , $only_id , $nombre = "" ) {
        $elementos = $data->categorias;
        if( $elementos->isNotEmpty() ) {
            foreach( $elementos AS $h ) {
                if( empty( $only_id ) )
                    $hijos[] = [ "id" => $h->id , "txt" => "{$nombre}{$data->nombre} / {$h->nombre}" ];
                else
                    $hijos[] = $h;
                self::hijosRecursivo( $h , $hijos , $only_id , "{$data->nombre} / " );
            }
        }
    }
    public function padres( $only_id = null ) {
        $padres = [];
        self::padresRecursivo( $this , $padres , $only_id );
        return array_reverse( $padres );
    }
    public function hijos( $only_id = null ) {
        $hijos = [];
        self::hijosRecursivo( $this , $hijos , $only_id );
        return $hijos;
    }
}
