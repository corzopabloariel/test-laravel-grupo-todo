<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Exception;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use App\Categoria;
use App\ProductoCategoria;
use App\Producto;
//search / show
class CategoriaController extends Controller
{
    public function index()
    {

        $categorias = Categoria::all();
        return view('admin.categoria.index', ['categorias' => $categorias,'categoriasRoot' => Categoria::whereNull("categoria_id")->get()]);
    }

    public function categoriaPorId($id)
    {

        $categoria = Categoria::find($id);
        $padres = [];
        if( !empty( $categoria->categoria_id ) )
            $padres = $categoria->padres( );
        return json_encode(array('success' => true, "error" => 0, "msg" => "OK", 'categoria' => $categoria , 'padres' => $padres ));
    }

    public function categoriaHijos($id)
    {
        return Categoria::find( $id )->categorias;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $rules = array(
            'nombre'   => 'required|min:5|max:50',
            'descripcion'   => 'min:5|max:150',
            'categoria_id'  => 'nullable'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return json_encode(array("error" => 1, "msg" => "Error al guardar"));
        } else {

            try {
                // store categoria
                $categoria = new Categoria;
                $categoria->nombre       = $request->nombre;
                $categoria->descripcion  = $request->descripcion;
                $categoria->categoria_id = $request->categoria_id;
                $categoria->save();

            } catch (Exception $e) {
                return json_encode(array("error" => 1, "msg" => $e->getMessage()));
            }

            return json_encode(array('success' => true, "error" => 0, "msg" => "OK"));
        }
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {

        $rules = array(
            'nombre'   => 'required|min:5|max:50',
            'id'   => 'required',
            'descripcion'   => 'min:5|max:150',
            'categoria_id'  => 'nullable'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return json_encode(array("error" => 1, "msg" => "Error al guardar"));
        } else {

            try {
                // update categoria
                Categoria::where('id', $request->id)->update(['nombre' => $request->nombre, 'descripcion' => $request->descripcion, 'categoria_id' => $request->categoria_id]);
            } catch (Exception $e) {
                return json_encode(array("error" => 1, "msg" => $e->getMessage()));
            }

            return json_encode(array('success' => true, "error" => 0, "msg" => "OK"));
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // delete
        try {
            $categoria = Categoria::find($id);
            $categoria->delete();
            return json_encode(array('success' => true, "error" => 0, "msg" => "OK"));
        } catch (\Exception $e) {
            return json_encode(array("error" => 1, "msg" => $e->getMessage()));
        }
    }


    /**
     * Listado de categorias
     */
    public function listado()
    {
        $categorias = Categoria::all();
        return view('front.categoria.index', ['categorias' => $categorias]);
    }


    /**
     * Detalle de categoria
     */
    public function detalle( $id )
    {

        $categoria = Categoria::find($id);
        $categoria_productos = ProductoCategoria::where('id_categoria', $id)->get();

        $productos = [];
        foreach($categoria_productos as $categoria_producto){
            if(isset($categoria_producto->id_producto)){
                $producto = Producto::find($categoria_producto->id_producto);

                if (isset($producto->nombre)) {
                    $productos[] = $producto;
                }
            }
        }

        $categoria->productos = $productos;

        $categorias = Categoria::whereNull("categoria_id")->get();

        return view('front.categoria.detail', ['categoria' => $categoria, 'categorias' => $categorias ]);

    }

}
