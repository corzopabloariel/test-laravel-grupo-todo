<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    protected $fillable = [
        "nombre",
        "descripcion"
    ];
    public function categorias()
    {
        return $this->hasMany('App\ProductoCategoria', 'id_producto' , 'id');
    }
}
