<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductoCategoriasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('producto_categorias', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger( 'id_producto' );
            $table->unsignedBigInteger( 'id_categoria' );
            $table->timestamp('updated_at')->useCurrent();
            $table->timestamp('created_at')->useCurrent();

            $table->foreign( 'id_categoria' )->references( 'id' )->on( 'categorias' )->onDelete( 'cascade' );
            $table->foreign( 'id_producto' )->references( 'id' )->on( 'productos' )->onDelete( 'cascade' );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('producto_categorias');
    }
}
