@section('headTitle', __('Error 404'))
@include('front.layouts.head_front')
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                @include('front.layouts.menu_front')
            </div>
            <div class="content">
                <div class="title m-b-md">
                    Página no encontrada
                </div>
            </div>
        </div>
    </div>
@include('front.layouts.footer_front')