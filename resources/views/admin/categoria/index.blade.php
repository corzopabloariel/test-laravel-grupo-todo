@section('headTitle', __('ABM Categoría'))
@include('admin.layouts.head_admin')

        <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                 @include('admin.layouts.menu_admin')
            </div>
            <div class="content">
                <div class="title m-b-md">
                    ABM Categorias
                </div>

                <div class="links">

                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            @if(isset($success))
                                <div class="alert alert-success">
                                    {{ $success }}
                                </div>
                            @endif

                            <div class="portlet box blue">

                                <div class="portlet-body">
                                    <div class="table-responsive">
                                        <table class="table table-bordered">
                                            <thead>
                                            <tr>
                                                <th>Nro</th>
                                                <th>Nombre</th>
                                                <th>Padre</th>
                                                <th>Edit</th>
                                                <th>Delete</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($categorias as $categoria)
                                                <tr>
                                                    <td>
                                                        {{ $categoria->id }}
                                                    </td>
                                                    <td>
                                                        <i class="fa fa-star"></i>
                                                        {{ $categoria->nombre }}
                                                    </td>
                                                    <td>
                                                        @php
                                                        $padres = $categoria->padres(1);
                                                        $h = "";
                                                        if( !empty( $padres ) ) {
                                                            foreach( $padres AS $p ) {
                                                                if( !empty( $h ) )
                                                                    $h .= " / ";
                                                                $h .= $p->nombre;
                                                            }
                                                        }
                                                        @endphp
                                                        {{ $h }}
                                                    </td>
                                                    <td>
														<button type="button" class="btn btn-info " id="ajaxEdit" onclick="editCategoria({{ $categoria->id }});"><i class="fa fa-edit"></i> Edit</button>
                                                    </td>
                                                    <td>
														<button type="button" class="btn btn-danger" id="ajaxDelete" onclick="deleteCategoria({{ $categoria->id }});"><i class="fa fa-trash-o"></i> Delete</button>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                         <!-- Trigger the modal with a button -->
                                         <button type="button" class="btn btn-info btn-sd" data-toggle="modal" onclick="clearCategoria();" data-target="#myModal" id="open">Add Categoria</button>

                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>

  <!-- MODDAL -->

  <div class="container">
            <form onsubmit="event.preventDefault();" method="post" action="{{ url('admin/agregarCategoria') }}" id="form">
                    @csrf
            <!-- Modal -->
            <div class="modal" tabindex="-1" role="dialog" id="myModal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="alert alert-danger" style="display:none"></div>
                <div class="modal-header">

                    <h5 class="modal-title">New Categoria</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group col-md-10">
                        <label for="nombre">Nombre:</label>
                        <input type="text" class="form-control" name="nombre" id="nombre" maxlength="50" minlength="5" required>
                        <input type="hidden" class="form-control" name="id" id="id" value="">
                        <small class="form-text text-muted">Entre 5 y 50 caracteres.</small>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-10">
                            <button type="button" class="btn btn-danger" onclick="limpiarCategorias( this );">Limpiar categorias</button>
                            <label for="categoria_id" id="label-categoria">Categoría:</label>
                            <select onchange="buscarCategorias( this );" class="form-control" name="categoria_id" id="categoria_id">
                                <option value="" hidden selected>Seleccionar Categoría</option>
                                @foreach( $categoriasRoot AS $c )
                                <option value="{{ $c->id }}">{{ $c->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-md-10">
                            <label for="description">Descripcion:</label>
                            <input type="text" class="form-control" name="descripcion" maxlength="150" minlength="5" id="descripcion">
                            <small class="form-text text-muted">Entre 5 y 150 caracteres.</small>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button  class="btn btn-success" id="ajaxSubmit">Save changes</button>
                    </div>
                </div>
            </div>
            </div>
            </form>
        </div>
        <!-- /Attachment Modal -->
        <script src="{{ asset('js/axios.min.js') }}"></script>
        <script>
            limpiarCategorias = ( t ) => {
                if( confirm( "Seguro de limpiar categorias?" ) ) {
                    document.getElementById("categoria_id").innerHTML = localStorage.select;
                    document.getElementById("label-categoria").innerHTML = "Categoría:";
                }
            };
            buscarCategorias = ( t ) => {
                let value = t.value;
                if( value != "" ) {
                    let text = t.options[t.selectedIndex].text;
                    if( value == "" )
                        return false;
                    if( value === null )
                        return false;
                    window.categoria_id_select = value;
                    axios({
                        method: "GET",
                        url: `{{ url('/admin/categoriaHijos') }}/${value}`,
                        responseType: 'json',
                        config: { headers: {'Content-Type': 'multipart/form-data' }}
                    })
                    .then((res) => {
                        if( res.data.length != 0 ) {
                            let select = document.getElementById("categoria_id");
                            document.getElementById("label-categoria").insertAdjacentHTML('beforeend', `<br/>${text}`);
                            select.innerHTML = '<option value="" hidden selected>Seleccionar Categoría</option>';
                            res.data.forEach( x => {
                                var opt = document.createElement("option");
                                opt.value = x.id;
                                opt.text = x.nombre;
                                select.add( opt , null )
                            } );
                            setTimeout(() => {
                                console.log(window.select_id_categorias)
                                if( window.select_id_categorias !== undefined ) {
                                    if( window.select_id_categorias.length > 0 ) {
                                        jQuery('#categoria_id').val(window.select_id_categorias[0]).trigger("change");
                                        window.select_id_categorias.shift();
                                    } else
                                        delete window.select_id_categorias;
                                }
                            }, 600);
                        }
                    })
                    .catch((err) => {
                        console.error( `ERROR en buscar categoría` );
                    })
                    .then(() => {});
                }
            };
            $('#myModal').on('hidden.bs.modal', function (e) {
                jQuery(`#categoria_id option`).prop( "disabled" , false );
            });
            function clearCategoria(){
                jQuery('#id').val("");
                jQuery('#nombre').val("");
                jQuery('#descripcion').val("");
            }
            function deleteCategoria(id){
                if(!confirm('Are you sure you want to delete this item?'))
                    return false;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                });
                jQuery.ajax({
                    url: "{{ url('/admin/eliminarCategoria') }}/" + id,
                    method: 'delete',
                    data: {
                        id: id,
                        _token: jQuery('[name="_token"]').val(),
                    },
                    success: function(result){

                        var obj = jQuery.parseJSON( result );

                        if(obj.error == 1)
                        {
                            jQuery('.alert-danger').html('');
                            jQuery('.alert-danger').show();
                            jQuery('.alert-danger').append('<li>'+obj.value+'</li>');

                        }
                        else
                        {
                            jQuery('.alert-danger').hide();
                            $('#open').hide();
                            $('#myModal').modal('hide');
                            location.reload();
                        }
                }});
            }
            function editCategoria(id){
                jQuery.ajax({
                        url: "{{ url('/admin/categoriaPorId') }}/"+id,
                        method: 'get',
                        data: {
                            id: id,
                            _token: jQuery('[name="_token"]').val(),
                        },
                        success: function(result){

                            var obj = jQuery.parseJSON( result );

                            if(obj.error == 1)
                            {
                                jQuery('.alert-danger').html('');
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<li>'+obj.value+'</li>');

                            }
                            else
                            {
                                if( jQuery(`#categoria_id option[value="${obj.categoria.id}"]`).length )
                                    jQuery(`#categoria_id option[value="${obj.categoria.id}"]`).prop( "disabled" , true );
                                jQuery('#id').val(obj.categoria.id);
                                jQuery('#nombre').val(obj.categoria.nombre);
                                jQuery('#descripcion').val(obj.categoria.descripcion);
                                if(obj.padres.length > 0) {
                                    window.select_id_categorias = obj.padres;
                                    jQuery('#categoria_id').val(window.select_id_categorias[0]).trigger("change");
                                    window.select_id_categorias.shift();
                                }
                                $('#myModal').modal('show');
                            }
                        }
                    });
            }

            $(document).ready(function(){
                if( localStorage.select === undefined )
                    localStorage.setItem( "select" , document.getElementById("categoria_id").innerHTML)
                jQuery('#ajaxSubmit').click(function(e){
                    e.preventDefault();
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    });
                    let categoria_id = jQuery('#categoria_id').val() == "" ? ( window.categoria_id_select !== undefined ? window.categoria_id_select : null) : jQuery('#categoria_id').val();
                    jQuery.ajax({
                        url: jQuery('#id').val()?"{{url('/admin/actualizarCategoria')}}":"{{url('/admin/agregarCategoria')}}",
                        method: 'post',
                        data: {
                            id: jQuery('#id').val()?jQuery('#id').val():0,
                            nombre: jQuery('#nombre').val(),
                            descripcion: jQuery('#descripcion').val(),
                            categoria_id: categoria_id,
                            _token: jQuery('[name="_token"]').val(),
                        },
                        success: function(result){
                            var obj = jQuery.parseJSON( result );
                            if(obj.error)
                            {
                                jQuery('.alert-danger').html('');
                                jQuery('.alert-danger').show();
                                jQuery('.alert-danger').append('<p>'+obj.msg+'</p>');

                            }
                            else
                            {
                                jQuery('.alert-danger').hide();
                                $('#open').hide();
                                $('#myModal').modal('hide');
                                jQuery('#id').val();
                                jQuery('#nombre').val();
                                jQuery('#descripcion').val();
                                location.reload();
                            }
                        }
                    });
                });

            });
        </script>
    @include('admin.layouts.footer_admin')
