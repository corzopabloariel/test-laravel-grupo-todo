<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Login</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <script src="https://kit.fontawesome.com/773d0bcede.js"></script>
        <link rel="stylesheet" href="css/admin.css">
    </head>
    <body>
@if($errors->any())
    <div class="position-fixed w-100 text-center" style="z-index:9999; top: 0;">
        <div class="alert alert-danger alert-dismissible position-absolute fade show d-inline-block mb-0" style="top: 0; left: 40%;">
            {!! $errors->first('mssg') !!}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
    </div>
@endif
        <main class="position-relative d-flex justify-content-center align-items-center full-height">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-sm-9 col-md-7 col-lg-5">
                        <div class="card bg-light shadow">
                            <div class="card-body">
                                <h3 class="card-title m-b-md title text-center">Panel Administrativo</h3>
                                <form method="POST" action="{{ route('login') }}">
                                    @csrf
                                    <div class="form-label-group">
                                        <label for="email"><i class="fas fa-user mr-2"></i>Email</label>
                                        <input type="email" name="email" id="email" class="form-control" placeholder="Email" required autofocus value="{{ old('email') }}">
                                        @if ($errors->has('email'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('email') }}</strong>
                                            </span>
                                        @endif
                                    </div>

                                    <div class="form-label-group">
                                        <label for="password"><i class="fas fa-key mr-2"></i>Contraseña</label>
                                        <input type="password" name="password" id="password" class="form-control" placeholder="Contraseña" required>
                                        @if ($errors->has('password'))
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="form-label-group">
                                        <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Acceso<i class="fas fa-sign-in-alt ml-2"></i></button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
    </body>
</html>