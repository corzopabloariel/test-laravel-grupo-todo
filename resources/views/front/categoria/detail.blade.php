@section('headTitle', __('Detalle de categoría: ' . $categoria->nombre))
@include('front.layouts.head_front')
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                 @include('front.layouts.menu_front')
            </div>
            <div class="content menu-categorias">
                <div class= "row">
                    <ul>
                        @foreach($categorias as $categoria_listado)
                            @include('front.layouts.menu',[ "data" => $categoria_listado ])
                        @endforeach
                    </ul>
                </div>
            </div>

            <div class="content">
                <div class="title m-b-md">
                  --  {{ $categoria->nombre }} --
                </div>
                <div class="title m-b-md">
                    @include('front.layouts.breadcrumb',[ "data" => $categoria , "tipo" => 0 ] )
                </div>
                <div class="links">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(isset($success))
                                <div class="alert alert-success">
                                    {{ $success }}
                                </div>
                            @endif
                            <div class="portlet box blue">
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                            <ul class="categorias">
                                                    {{ $categoria->decripcion }}
                                                    <p>Productos: </p>
                                                    @forelse( $categoria->productos as $producto )
                                                        <li> <a href = "{{  url('front/producto') }}/{{$producto->id }}" > {{ $producto->nombre }} </a></li>
                                                        @empty
                                                        <li>SIN REGISTROS</li>
                                                    @endforelse
                                            </ul>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>

    @include('front.layouts.footer_front')
