@include('front.layouts.head_front')
    <body>
        <div class="flex-center position-ref full-height">
            <div class="top-right links">
                 @include('front.layouts.menu_front')
            </div>
             <div class="content menu-categorias">
                <div class= "row">
                    <ul>
                        @foreach($categorias as $categoria)
                            @include('front.layouts.menu',[ "data" => $categoria ])
                        @endforeach
                    </ul>
                </div>
            </div>


            <div class="content">
                <div class="title m-b-md">
                  --  {{ $producto->nombre }} --
                </div>
                <div class="links">
                            @if ($errors->any())
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            @if(isset($success))
                                <div class="alert alert-success">
                                    {{ $success }}
                                </div>
                            @endif
                            <div class="portlet box blue">
                                <div class="portlet-body">
                                    <div class="table-responsive">
                                                <p>id : {{ $producto->id }}</p>
                                                <p>descripción: {{ $producto->descripcion }}</p>
                                                <ul class="categorias">categorias:
                                                    @foreach( $producto->categorias as $categoria )
                                                        @php
                                                        $padres = $categoria->padres(1);
                                                        $h = "";
                                                        if( !empty( $padres ) ) {
                                                            foreach( $padres AS $p ) {
                                                                if( !empty( $h ) )
                                                                    $h .= " / ";
                                                                $h .= $p->nombre;
                                                            }
                                                        }
                                                        $h = empty( $h ) ? $categoria->nombre : "{$h} / {$categoria->nombre}";
                                                        @endphp
                                                        <li> <a href = "{{  url('front/categoria') }}/{{$categoria->id }}" > {{ $h }} </a></li>
                                                    @endforeach
                                                </ul>
                                    </div>
                                </div>
                            </div>
                </div>
            </div>
        </div>

    @include('front.layouts.footer_front')
