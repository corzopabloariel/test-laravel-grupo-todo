@php
$b_categorias = $data->padres(1);
$h = "";
foreach($b_categorias AS $c) {
    if(!empty($h) )
        $h .= " > ";
    $h .= $c->nombre;
}
$h .= (empty( $h ) ? "" : " > " ) . "<strong>{$data->nombre}</strong>";
@endphp
{!! $h !!}