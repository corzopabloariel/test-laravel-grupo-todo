@php
$hijos = $data->categorias;
@endphp
<li>
    <a href = "{{  url('front/categoria') }}/{{$data->id }}" >{{ $data->nombre }}</a>
    @if( !empty($hijos) )
        <ul>
            @foreach( $hijos AS $h )
                @include('front.layouts.menu',[ "data" => $h ])
            @endforeach
        </ul>
    @endif
</li>